package com.muscularcandy67;

public class Rettangolo {

    private double base;
    private double altezza;

    public Rettangolo(double base, double altezza) {
        setBase(base);
        setAltezza(altezza);
    }

    public double getBase() {
        return base;
    }

    public boolean setBase(double base) {
        if(base>=0){
            this.base=base;
            return true;
        }
        return false;
    }

    public double getAltezza() {
        return altezza;
    }

    public boolean setAltezza(double altezza) {
        if(altezza>=0){
            this.altezza = altezza;
            return true;
        }
        return false;
    }

    public double getPerimetro(){
        return base + base + altezza +altezza;
    }

    public double getArea(){
        return base*altezza;
    }
}

